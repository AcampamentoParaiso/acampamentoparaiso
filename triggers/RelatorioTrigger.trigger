trigger RelatorioTrigger on Relatorio__c (before insert, before update) {
    if(Trigger.isBefore){
        if (Trigger.isInsert) {
            RelatorioClass.ValidarRecordType();
            RelatorioClass.TotalRelatorio();
        }
        if (Trigger.isUpdate) {
            RelatorioClass.ValidarRecordType();
            RelatorioClass.TotalRelatorio();
        }
    }
}