trigger AvaliacaoTrigger on Avaliacao__c (before insert, before update, after insert, after update) {    
    if(Trigger.isBefore){
        if (Trigger.isInsert) {
            AvaliacaoClass.ValidarRecordType();
            AvaliacaoClass.NameAvaliacao();
            AvaliacaoClass.TotalAvaliacao();
        }
        if (Trigger.isUpdate) {
            AvaliacaoClass.ValidarRecordType();
            AvaliacaoClass.TotalAvaliacao();
        }
    }/*
    if(Trigger.isAfter){
        if (Trigger.isInsert) {
            AvaliacaoClass.IntegrarDados();
        }
        if (Trigger.isUpdate) {
            AvaliacaoClass.IntegrarDados();
        }
    }*/
}