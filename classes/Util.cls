public class Util {
    
    private static Map<String, Equipantes__c> mapEquipApoio = new Map<String, Equipantes__c>(GetApoioEqui());
    private static Map<String, Equipantes__c> mapEquipConselo = new Map<String, Equipantes__c>(GetConseloEqui());
    private static Map<String, Equipantes__c> mapEquipConsela = new Map<String, Equipantes__c>(GetConselaEqui());
    
    private static Map<Id, RecordType> mapRecordTypeId = new Map<Id, RecordType>([SELECT id, DeveloperName
                                                                                    FROM RecordType
                                                                                   WHERE SobjectType =: GlobalConst.AvaliacaoC]);
    
    public static void ResultsRestAvaliacao(HttpResponse response){
        List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());
        List<Avaliacao__c> listAval = new List<Avaliacao__c>();
        for(Object loopResult : results){
            Avaliacao__c aval = new Avaliacao__c();
            Map<String, Object> mapResult = new Map<String, Object>((Map<String, Object>)loopResult);
            Map<String, Object> mapRecordType = new Map<String, Object>((Map<String, Object>)mapResult.get(GlobalConst.RecordTypeJ));
            if(GlobalConst.Apoio==(String) mapRecordType.get(GlobalConst.DeveloperNameJ)){
                listAval.add(Util.ApoioJson(mapResult));
            }else if(GlobalConst.Consela==(String) mapRecordType.get(GlobalConst.DeveloperNameJ)){
                listAval.add(Util.ConselaJson(mapResult));
            }else if(GlobalConst.Conselo==(String) mapRecordType.get(GlobalConst.DeveloperNameJ)){
                listAval.add(Util.ConseloJson(mapResult));
            }
        }
        Upsert listAval;
    }
    
    
    public static Avaliacao__c ApoioJson(Map<String, Object> jsonResult){
        if(ValIdExt((String)jsonResult.get(GlobalConst.NameCJ))){
            Map<String, Object> mapEquipante = new Map<String, Object>((Map<String, Object>)jsonResult.get(GlobalConst.EquipanteRJ));
            Avaliacao__c aval = new Avaliacao__c();
            aval.Equipante__c = mapEquipApoio.get((String)mapEquipante.get(GlobalConst.NameJ)).Id;
            for(Id loopRecordType : mapRecordTypeId.keySet()){
                if(mapRecordTypeId.get(loopRecordType).DeveloperName == GlobalConst.Apoio){
                    aval.RecordTypeId = loopRecordType;
                }
            }
            aval.Name__c = (String)jsonResult.get(GlobalConst.NameCJ);
            aval.Horario__c = (String)jsonResult.get(GlobalConst.HorarioCJ);
            aval.Submissao_Chefia__c = (String)jsonResult.get(GlobalConst.SubmissaoChefiaCJ);
            aval.Envolvimento_com_Acampantes__c = (String)jsonResult.get(GlobalConst.EnvolvimentoAcampantesCJ);
            aval.Observacao__c = (String)jsonResult.get(GlobalConst.ObservacaoCJ);
            aval.Deploy__c = true;
            return aval;
        }else{
            return null;
        }
    }
    
    public static Avaliacao__c ConseloJson(Map<String, Object> jsonResult){
        if(ValIdExt((String)jsonResult.get(GlobalConst.NameCJ))){
            Map<String, Object> mapEquipante = new Map<String, Object>((Map<String, Object>)jsonResult.get(GlobalConst.EquipanteRJ));
            Avaliacao__c aval = new Avaliacao__c();
            aval.Equipante__c = mapEquipConselo.get((String)mapEquipante.get(GlobalConst.NameJ)).Id;
            for(Id loopRecordType : mapRecordTypeId.keySet()){
                if(mapRecordTypeId.get(loopRecordType).DeveloperName == GlobalConst.Conselo){
                    aval.RecordTypeId = loopRecordType;
                }
            }
            aval.Name__c = (String)jsonResult.get(GlobalConst.NameCJ);
            aval.Horario__c = (String)jsonResult.get(GlobalConst.HorarioCJ);
            aval.Submissao_Chefia__c = (String)jsonResult.get(GlobalConst.SubmissaoChefiaCJ);
            aval.Buscar__c = (String)jsonResult.get(GlobalConst.BuscarCJ);
            aval.Brincar__c = (String)jsonResult.get(GlobalConst.BrincarCJ);
            aval.Bem_dizer__c = (String)jsonResult.get(GlobalConst.BemDizerCJ);
            aval.Observacao__c = (String)jsonResult.get(GlobalConst.ObservacaoCJ);
            aval.Deploy__c = true;
            return aval;
        }else{
            return null;
        }
    }
    
    public static Avaliacao__c ConselaJson(Map<String, Object> jsonResult){
        if(ValIdExt((String)jsonResult.get(GlobalConst.NameCJ))){
            Map<String, Object> mapEquipante = new Map<String, Object>((Map<String, Object>)jsonResult.get(GlobalConst.EquipanteRJ));
            Avaliacao__c aval = new Avaliacao__c();
            aval.Equipante__c = mapEquipConsela.get((String)mapEquipante.get(GlobalConst.NameJ)).Id;
            for(Id loopRecordType : mapRecordTypeId.keySet()){
                if(mapRecordTypeId.get(loopRecordType).DeveloperName == GlobalConst.Consela){
                    aval.RecordTypeId = loopRecordType;
                }
            }
            aval.Name__c = (String)jsonResult.get(GlobalConst.NameCJ);
            aval.Horario__c = (String)jsonResult.get(GlobalConst.HorarioCJ);
            aval.Submissao_Chefia__c = (String)jsonResult.get(GlobalConst.SubmissaoChefiaCJ);
            aval.Buscar__c = (String)jsonResult.get(GlobalConst.BuscarCJ);
            aval.Brincar__c = (String)jsonResult.get(GlobalConst.BrincarCJ);
            aval.Bem_dizer__c = (String)jsonResult.get(GlobalConst.BemDizerCJ);
            aval.Observacao__c = (String)jsonResult.get(GlobalConst.ObservacaoCJ);
            aval.Deploy__c = true;
            return aval;
        }else{
            return null;
        }
    }
    
    public static Map<String, Equipantes__c> GetApoioEqui(){
        Map<String, Equipantes__c> mapApoio = new Map<String, Equipantes__c>();
        List<Equipantes__c> listEquip = [SELECT Id,Name,Funcao__c,Novato__c,RecordType.DeveloperName
                                           FROM Equipantes__c
                                          WHERE RecordType.DeveloperName =: GlobalConst.Apoio];
        
        for(Equipantes__c loopEqui : listEquip){
            mapApoio.put(loopEqui.Name, loopEqui);
        }
        
        return mapApoio;
    }
    
    public static Map<String, Equipantes__c> GetConseloEqui(){
        Map<String, Equipantes__c> mapConselheiro = new Map<String, Equipantes__c>();
        List<Equipantes__c> listEquip = [SELECT Id,Name,Funcao__c,Novato__c,RecordType.DeveloperName
                                           FROM Equipantes__c
                                          WHERE RecordType.DeveloperName =: GlobalConst.Conselo];
        
        for(Equipantes__c loopEqui : listEquip){
            mapConselheiro.put(loopEqui.Name, loopEqui);
        }
        
        return mapConselheiro;
    }
    
    public static Map<String, Equipantes__c> GetConselaEqui(){
        Map<String, Equipantes__c> mapConselheira = new Map<String, Equipantes__c>();
        List<Equipantes__c> listEquip = [SELECT Id,Name,Funcao__c,Novato__c,RecordType.DeveloperName
                                           FROM Equipantes__c
                                          WHERE RecordType.DeveloperName =: GlobalConst.Consela];
        
        for(Equipantes__c loopEqui : listEquip){
            mapConselheira.put(loopEqui.Name, loopEqui);
        }
        
        return mapConselheira;
    }
    
    public static Boolean ValIdExt(String codExt){
        List<Avaliacao__c> listAval = [SELECT id,name
                                         FROM Avaliacao__c
                                        WHERE Name_ExlId__c =: codExt];
        return true/*(listAval.size()>=1?true:false)*/;
    }
}