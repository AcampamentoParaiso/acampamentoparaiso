@RestResource(urlMapping='/Avaliacao/*')
global with sharing class RestAvaliacao {
    public static List<Avaliacao__c> listAval = new List<Avaliacao__c>();
    
    @HttpGet
    global static List<Avaliacao__c> getRecord() {
        RestRequest request = RestContext.request;
        String avalRec = request.requestURI.substring(
            request.requestURI.lastIndexOf('/')+1);
        if(String.isEmpty(avalRec)){
            listAval.addAll(getApoio());
            listAval.addAll(getConselheira());
            listAval.addAll(getConselheiro());
            return listAval;
        }else if(avalRec.equalsIgnoreCase('New')){
            return null;
        }else{
            return null;
        }
    }
    
    private static List<Avaliacao__c> getApoio(){
        List<Avaliacao__c> listAvalApoio = [SELECT Name__c, Envolvimento_com_Acampantes__c,
                                                   Horario__c, Submissao_Chefia__c, Total__c,
                                                   RecordType.DeveloperName, Observacao__c,
                                                   Equipante__r.Name
                                              FROM Avaliacao__c
                                             WHERE RecordType.DeveloperName =: GlobalConst.Apoio];
        return listAvalApoio;
    }
    
    private static List<Avaliacao__c> getConselheira(){
        List<Avaliacao__c> listAvalConselheira = [SELECT Name__c, Submissao_Chefia__c,
                                                         Horario__c, Total__c, Observacao__c,
                                                         Bem_dizer__c, Brincar__c, Buscar__c,
                                                         RecordType.DeveloperName,
                                                         Equipante__r.Name
                                                    FROM Avaliacao__c
                                                   WHERE RecordType.DeveloperName =: GlobalConst.Consela];
        return listAvalConselheira;
    }

    private static List<Avaliacao__c> getConselheiro(){
        List<Avaliacao__c> listAvalConselheiro = [SELECT Name__c, Submissao_Chefia__c,
                                                         Horario__c, Total__c, Observacao__c,
                                                         Bem_dizer__c, Brincar__c, Buscar__c,
                                                         RecordType.DeveloperName,
                                                         Equipante__r.Name
                                                    FROM Avaliacao__c
                                                   WHERE RecordType.DeveloperName =: GlobalConst.Conselo];
        return listAvalConselheiro;
    }
}