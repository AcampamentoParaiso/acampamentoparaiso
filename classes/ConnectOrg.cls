public class ConnectOrg {
    
    private static Map<Id, RecordType> mapRcTpEquipId = new Map<Id, RecordType>([SELECT id, DeveloperName
                                                                                    FROM RecordType
                                                                                   WHERE SobjectType = 'Equipantes__c']);
    
    private static Map<String, Equipantes__c> mapEquipApoio = new Map<String, Equipantes__c>();
    private static Map<String, Equipantes__c> mapEquipConselo = new Map<String, Equipantes__c>();
    private static Map<String, Equipantes__c> mapEquipConsela = new Map<String, Equipantes__c>();
    
    public static String OAuth(String consKey, String consSct, String login, String pass, String endpoint){
        String oauth;
        String baseUrl = endpoint + GlobalConst.EndPoint_Auth;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        request.setEndpoint(baseUrl);
        request.setMethod(GlobalConst.POST);
        String body = 'grant_type='+EncodingUtil.urlEncode(GlobalConst.passwordJ, GlobalConst.UTF8)
            +'&client_id='+EncodingUtil.urlEncode(consKey, GlobalConst.UTF8)
            +'&client_secret='+EncodingUtil.urlEncode(consSct, GlobalConst.UTF8)
            +'&username='+EncodingUtil.urlEncode(login, GlobalConst.UTF8)
            +'&password='+EncodingUtil.urlEncode(pass, GlobalConst.UTF8);
        request.setBody(body);
        response = http.send(request);
System.debug('StatusCode: '+response.getStatusCode());
System.debug('Body: '+response.getBody());
        if(response.getStatusCode()==200){
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            oauth =  (String) results.get(GlobalConst.AccessTokenJ);
        }
System.debug('OAuth: '+oauth);
        return oauth;
    }
    
    public static void RestAvaliacao(String oAuth, String endpoint, String pointAc){
        String baseUrl = endpoint + GlobalConst.EndPoint_Aval;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        request.setEndpoint(baseUrl);
        request.setMethod(GlobalConst.GET);
        request.setHeader(GlobalConst.contTypeJ, GlobalConst.appJsonJ);
        request.setHeader(GlobalConst.authoJ, GlobalConst.oAuthJ+oAuth);
        response = http.send(request);
System.debug('StatusCode: '+response.getStatusCode());
System.debug('Body: '+response.getBody());
        if(response.getStatusCode()==200){
            UpdEquip(oAuth, endpoint);
            Util.ResultsRestAvaliacao(response);
        }
    }
    
    private static void UpdEquip(String oAuth, String endpoint){
        mapEquipApoio = new Map<String, Equipantes__c>(Util.GetApoioEqui());
        mapEquipConselo = new Map<String, Equipantes__c>(Util.GetConseloEqui());
        mapEquipConsela = new Map<String, Equipantes__c>(Util.GetConselaEqui());
        String baseUrl = endpoint + GlobalConst.EndPoint_Equi;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        request.setEndpoint(baseUrl);
        request.setMethod(GlobalConst.GET);
        request.setHeader(GlobalConst.contTypeJ, GlobalConst.appJsonJ);
        request.setHeader(GlobalConst.authoJ, GlobalConst.oAuthJ+oAuth);
        response = http.send(request);
        if(response.getStatusCode()==200){
            List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());
            List<Equipantes__c> listEqui = new List<Equipantes__c>();
            for(Object loopResult : results){
                Equipantes__c equi = new Equipantes__c();
                Map<String, Object> mapResult = new Map<String, Object>((Map<String, Object>)loopResult);
                Map<String, Object> mapRecordType = new Map<String, Object>((Map<String, Object>)mapResult.get(GlobalConst.RecordTypeJ));
                if(GlobalConst.Apoio==(String)mapRecordType.get(GlobalConst.DeveloperNameJ)){
                    if(mapEquipApoio.get((String)mapResult.get(GlobalConst.NameJ))==null){
                        equi.Name = (String)mapResult.get(GlobalConst.NameJ);
                        equi.Funcao__c = (String)mapResult.get(GlobalConst.FuncaoCJ);
                        equi.Novato__c = (Boolean)mapResult.get(GlobalConst.NovatoCJ);
                        for(Id loopRecordType : mapRcTpEquipId.keySet()){
                            if(mapRcTpEquipId.get(loopRecordType).DeveloperName == GlobalConst.Apoio){
                                equi.RecordTypeId = loopRecordType;
                            }
                        }
                        listEqui.add(equi);
                    }
                }else if(GlobalConst.Consela==(String)mapRecordType.get(GlobalConst.DeveloperNameJ)){
                    if(mapEquipConsela.get((String)mapResult.get(GlobalConst.NameJ))==null){
                        equi.Name = (String)mapResult.get(GlobalConst.NameJ);
                        equi.Funcao__c = (String)mapResult.get(GlobalConst.FuncaoCJ);
                        equi.Novato__c = (Boolean)mapResult.get(GlobalConst.NovatoCJ);
                        for(Id loopRecordType : mapRcTpEquipId.keySet()){
                            if(mapRcTpEquipId.get(loopRecordType).DeveloperName == GlobalConst.Consela){
                                equi.RecordTypeId = loopRecordType;
                            }
                        }
                        listEqui.add(equi);
                    }
                }else if(GlobalConst.Conselo==(String)mapRecordType.get(GlobalConst.DeveloperNameJ)){
                    if(mapEquipConselo.get((String)mapResult.get(GlobalConst.NameJ))==null){
                        equi.Name = (String)mapResult.get(GlobalConst.NameJ);
                        equi.Funcao__c = (String)mapResult.get(GlobalConst.FuncaoCJ);
                        equi.Novato__c = (Boolean)mapResult.get(GlobalConst.NovatoCJ);
                        for(Id loopRecordType : mapRcTpEquipId.keySet()){
                            if(mapRcTpEquipId.get(loopRecordType).DeveloperName == GlobalConst.Conselo){
                                equi.RecordTypeId = loopRecordType;
                            }
                        }
                        listEqui.add(equi);
                    }
                }
            }
            INSERT listEqui;
        }
    }
}