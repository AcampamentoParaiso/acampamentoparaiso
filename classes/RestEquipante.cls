@RestResource(urlMapping='/Equipante/*')
global with sharing class RestEquipante {
    @HttpGet
    global static List<Equipantes__c> getRecord() {
        List<Equipantes__c> listEqui = [SELECT Name, Funcao__c, Novato__c, RecordType.DeveloperName
                                          FROM Equipantes__c];
        return listEqui;
    }
}