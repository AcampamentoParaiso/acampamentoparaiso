public class RelatorioClass {
    private static List<Relatorio__c> listRelatNew = new List<Relatorio__c>();
    
    public static void ValidarRecordType(){
        List<id> listIdEquip = new List<Id>();
        List<id> listRecordType = new List<Id>();
        listRelatNew.addAll((List<Relatorio__c>)trigger.new);
        
        for(Relatorio__c loopRelat : listRelatNew){
            listIdEquip.add(loopRelat.Equipante__c);
            listRecordType.add(loopRelat.RecordTypeId);
        }
        
        Map<Id, Equipantes__c> mapEquip = new Map<Id, Equipantes__c>([SELECT id, RecordType.DeveloperName
                                                                      FROM Equipantes__c
                                                                      WHERE id =: listIdEquip]);
        
        Map<Id, RecordType> mapRecType = new Map<Id, RecordType>([SELECT id, DeveloperName
                                                                    FROM RecordType
                                                                   WHERE id =: listRecordType]);
        
        for(Relatorio__c loopRelat : listRelatNew){
            if(mapEquip.get(loopRelat.Equipante__c).RecordType.DeveloperName !=
               mapRecType.get(loopRelat.RecordTypeId).DeveloperName){
                loopRelat.AddError('Voce esta tentando criar um Relatorio de um '+
                                   mapEquip.get(loopRelat.Equipante__c).RecordType.DeveloperName+
                                  ' como um Equipante do Tipo '+
                                   mapRecType.get(loopRelat.RecordTypeId).DeveloperName);
            }
        }
    }
    
    public static void NameRelatorio(){
        List<id> listIdEquip = new List<Id>();
        Date dataT = Date.today();
        listRelatNew.addAll((List<Relatorio__c>)trigger.new);
        
        for(Relatorio__c loopRelat : listRelatNew){
            listIdEquip.add(loopRelat.Equipante__c);
        }
        
        Map<Id, Equipantes__c> mapEquip = new Map<Id, Equipantes__c>([SELECT id, name
                                                                        FROM Equipantes__c
                                                                       WHERE id =: listIdEquip]);
        
        for(Relatorio__c loopRelat : listRelatNew){
            loopRelat.name__c = mapEquip.get(loopRelat.Equipante__c).name+'-'+(dataT.month()>9?String.valueOf(dataT.month()):'0'+String.valueOf(dataT.month()))+'/'+datat.day();
        }
    }
    
    public static void TotalRelatorio(){
        List<Id> listRecordTypeId = new List<Id>();
        Map<Id, Avaliacao__c> mapAvaliacao;
        List<id> listIdEquip = new List<Id>();
        Date dataT = Date.today();
        listRelatNew.addAll((List<Relatorio__c>)trigger.new);
        
        for(Relatorio__c loopRelat : listRelatNew){
            listRecordTypeId.add(loopRelat.RecordTypeId);
        }
        
        Map<Id, RecordType> mapRecordType = new Map<Id, RecordType>([SELECT DeveloperName
                                                                       FROM RecordType
                                                                     WHERE Id =: listRecordTypeId]);
        
        for(Relatorio__c loopRelat : listRelatNew){
            Integer bemDizer = 0;
            Integer brincar = 0;
            Integer buscar = 0;
            Integer envolAcamp = 0;
            Integer horario = 0;
            Integer submChefia = 0;
            String obs = '';
            
            if(mapRecordType.get(loopRelat.RecordTypeId).DeveloperName ==  'Apoio'){
                mapAvaliacao = new Map<Id, Avaliacao__c>([SELECT Id, Envolvimento_com_Acampantes__c, Horario__c, Submissao_Chefia__c, Observacao__c,  CreatedDate
                                                            FROM Avaliacao__c
                                                           WHERE Equipante__c =: loopRelat.Equipante__c
                                                             and CreatedDate >=: loopRelat.Inicio_Periodo__c
                                                             and CreatedDate <=: loopRelat.Fim_Periodo__c]);
                
                for(Id loopAvalId : mapAvaliacao.keySet()){
                    envolAcamp += (mapAvaliacao.get(loopAvalId).Envolvimento_com_Acampantes__c.equalsIgnoreCase('Ruim')?1:
                                      (mapAvaliacao.get(loopAvalId).Envolvimento_com_Acampantes__c.equalsIgnoreCase('Medio')?3:5));
                    horario += (mapAvaliacao.get(loopAvalId).Horario__c.equalsIgnoreCase('Ruim')?1:
                                   (mapAvaliacao.get(loopAvalId).Horario__c.equalsIgnoreCase('Medio')?3:5));
                    submChefia += (mapAvaliacao.get(loopAvalId).Submissao_Chefia__c.equalsIgnoreCase('Ruim')?1:
                                      (mapAvaliacao.get(loopAvalId).Submissao_Chefia__c.equalsIgnoreCase('Medio')?3:5));
                    obs = obs+String.valueOf(mapAvaliacao.get(loopAvalId).CreatedDate.date())+':\n';
                    obs =+ obs+mapAvaliacao.get(loopAvalId).Observacao__c+'\n\n';
                }
                if(mapAvaliacao.size()!=0){
                    loopRelat.Envolvimento_com_Acampantes__c = envolAcamp/mapAvaliacao.size();
                    loopRelat.Horario__c = horario/mapAvaliacao.size();
                    loopRelat.Submissao_Chefia__c = submChefia/mapAvaliacao.size();
                    loopRelat.Observacoes__c = obs;
                    loopRelat.Total__c = (loopRelat.Envolvimento_com_Acampantes__c+loopRelat.Horario__c+loopRelat.Submissao_Chefia__c)/3;
                }else{
                    loopRelat.AddError('Não Possui nenhuma avaliação dentro desse Periodo!');
                }
            }
        }
    }
}