public class AvaliacaoClass {
    private static List<Avaliacao__c> listAvalNew = new List<Avaliacao__c>();
    
    public static void ValidarRecordType(){
        List<id> listIdEquip = new List<Id>();
        List<id> listRecordType = new List<Id>();
        listAvalNew.addAll((List<Avaliacao__c>)trigger.new);
        
        for(Avaliacao__c loopAval : listAvalNew){
            listIdEquip.add(loopAval.Equipante__c);
            listRecordType.add(loopAval.RecordTypeId);
        }
        
        Map<Id, Equipantes__c> mapEquip = new Map<Id, Equipantes__c>([SELECT id, RecordType.DeveloperName
                                                                      FROM Equipantes__c
                                                                      WHERE id =: listIdEquip]);
        
        Map<Id, RecordType> mapRecType = new Map<Id, RecordType>([SELECT id, DeveloperName
                                                                    FROM RecordType
                                                                   WHERE id =: listRecordType]);
        
        for(Avaliacao__c loopAval : listAvalNew){
            if(mapEquip.get(loopAval.Equipante__c).RecordType.DeveloperName !=
               mapRecType.get(loopAval.RecordTypeId).DeveloperName){
                loopAval.AddError('Voce esta tentando Avaliar um '+
                                   mapEquip.get(loopAval.Equipante__c).RecordType.DeveloperName+
                                  ' como um Equipante do Tipo '+
                                   mapRecType.get(loopAval.RecordTypeId).DeveloperName);
            }
        }
    }
    
    public static void NameAvaliacao(){
        List<id> listIdEquip = new List<Id>();
        Date dataT = Date.today();
        listAvalNew.addAll((List<Avaliacao__c>)trigger.new);
        
        for(Avaliacao__c loopAval : listAvalNew){
            listIdEquip.add(loopAval.Equipante__c);
        }
        
        Map<Id, Equipantes__c> mapEquip = new Map<Id, Equipantes__c>([SELECT id, name
                                                                      FROM Equipantes__c
                                                                      WHERE id =: listIdEquip]);
        
        for(Avaliacao__c loopAval : listAvalNew){
            if(!loopAval.Deploy__c){
                loopAval.name__c = mapEquip.get(loopAval.Equipante__c).name+
                    '-'+
                    (dataT.month()>9?String.valueOf(dataT.month()):'0'+String.valueOf(dataT.month()))+
                    '/'+
                    (dataT.day()>9?String.valueOf(dataT.day()):'0'+String.valueOf(dataT.day()));
            }
        }
    }
    
    public static void TotalAvaliacao(){
        List<Id> listRecordTypeId = new List<Id>();
        listAvalNew.addAll((List<Avaliacao__c>)trigger.new);
        
        for(Avaliacao__c loopAval : listAvalNew){
            listRecordTypeId.add(loopAval.RecordTypeId);
        }
        
        Map<Id, RecordType> mapRecordType = new Map<Id, RecordType>([SELECT DeveloperName
                                                                       FROM RecordType
                                                                      WHERE Id =: listRecordTypeId]);
        
        for(Avaliacao__c loopAval : listAvalNew){
            Integer bemDizer = 0;
            Integer brincar = 0;
            Integer buscar = 0;
            Integer envolAcamp = 0;
            Integer horario = 0;
            Integer submChefia = 0;
            Integer acompEquip = 0;
            Integer resolProbl = 0;
            Integer comun = 0;
            Integer contrEquip = 0;
            
            if(mapRecordType.get(loopAval.RecordTypeId).DeveloperName ==  GlobalConst.Apoio){
                envolAcamp = (loopAval.Envolvimento_com_Acampantes__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Envolvimento_com_Acampantes__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                horario = (loopAval.Horario__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Horario__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                submChefia = (loopAval.Submissao_Chefia__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Submissao_Chefia__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                loopAval.Total__c = (envolAcamp+horario+submChefia)/3;
            }else if(mapRecordType.get(loopAval.RecordTypeId).DeveloperName ==  GlobalConst.Conselo || mapRecordType.get(loopAval.RecordTypeId).DeveloperName == GlobalConst.Consela){
                bemDizer = (loopAval.Bem_dizer__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Bem_dizer__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                brincar = (loopAval.Brincar__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Brincar__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                buscar = (loopAval.Buscar__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Buscar__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                horario = (loopAval.Horario__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Horario__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                submChefia = (loopAval.Submissao_Chefia__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Submissao_Chefia__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                loopAval.Total__c = (bemDizer+brincar+buscar+horario+submChefia)/5;
            }else if(mapRecordType.get(loopAval.RecordTypeId).DeveloperName ==  GlobalConst.Chefe){
                horario = (loopAval.Horario__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Horario__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                submChefia = (loopAval.Submissao_Chefia__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Submissao_Chefia__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                acompEquip = (loopAval.Acompanhamento_equipe__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Acompanhamento_equipe__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                resolProbl = (loopAval.Resulocao_Problemas__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Resulocao_Problemas__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                comun = (loopAval.Comunicacao__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Comunicacao__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                contrEquip = (loopAval.Controle_Equipe__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Controle_Equipe__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                loopAval.Total__c = (contrEquip+comun+resolProbl+acompEquip+horario+submChefia)/6;
            }else if(mapRecordType.get(loopAval.RecordTypeId).DeveloperName ==  GlobalConst.Outros){
                horario = (loopAval.Horario__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Horario__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                submChefia = (loopAval.Submissao_Chefia__c.equalsIgnoreCase(GlobalConst.Ruim)?1:(loopAval.Submissao_Chefia__c.equalsIgnoreCase(GlobalConst.Medio)?3:5));
                loopAval.Total__c = (horario+submChefia)/2;
            }
        }
    }
    /*
    public static void IntegrarDados(){
        List<Id> listRecordTypeId = new List<Id>();
        listAvalNew.addAll((List<Avaliacao__c>)trigger.new);
        Boolean blApoio = false;
        Boolean blConselo = false;
        Boolean blConsela = false;
        Boolean blChefe = false;
        Boolean blOutros = false;
        
        for(Avaliacao__c loopAval : listAvalNew){
            listRecordTypeId.add(loopAval.RecordTypeId);
        }
        
        Map<Id, RecordType> mapRecordType = new Map<Id, RecordType>([SELECT DeveloperName
                                                                       FROM RecordType
                                                                     WHERE Id =: listRecordTypeId]);
        
        for(Avaliacao__c loopAval : listAvalNew){
            if(mapRecordType.get(loopAval.RecordTypeId).DeveloperName ==  GlobalConst.Apoio){
                blApoio = true;
            }else if(mapRecordType.get(loopAval.RecordTypeId).DeveloperName ==  GlobalConst.Conselo){
                blConselo = true;
            }else if(mapRecordType.get(loopAval.RecordTypeId).DeveloperName ==  GlobalConst.Consela){
                blConsela = true;
            }else if(mapRecordType.get(loopAval.RecordTypeId).DeveloperName ==  GlobalConst.Chefe){
                blChefe = true;
            }else if(mapRecordType.get(loopAval.RecordTypeId).DeveloperName ==  GlobalConst.Outros){
                blOutros = true;
            }
        }
        
        if(blApoio){
            ConnectOrg.RestAvaliacao(ConnectOrg.OAuth(GlobalConst.ConsKeyDrt,
                                                      GlobalConst.ConsSctDrt,
                                                      GlobalConst.UsuarioDrt,
                                                      GlobalConst.PasswordDrt,
                                                      GlobalConst.EndPointDrt), GlobalConst.EndPointDrt, GlobalConst.EndPoint_NewApoio);
        }else if(blConselo){
            ConnectOrg.RestAvaliacao(ConnectOrg.OAuth(GlobalConst.ConsKeyDrt,
                                                      GlobalConst.ConsSctDrt,
                                                      GlobalConst.UsuarioDrt,
                                                      GlobalConst.PasswordDrt,
                                                      GlobalConst.EndPointDrt), GlobalConst.EndPointDrt, GlobalConst.EndPoint_NewConselo);
        }else if(blConsela){
            ConnectOrg.RestAvaliacao(ConnectOrg.OAuth(GlobalConst.ConsKeyDrt,
                                                      GlobalConst.ConsSctDrt,
                                                      GlobalConst.UsuarioDrt,
                                                      GlobalConst.PasswordDrt,
                                                      GlobalConst.EndPointDrt), GlobalConst.EndPointDrt, GlobalConst.EndPoint_NewConsela);
        //hefe nao entra na validacao, pois sera apenas avaliado pelo aplicativo de diretoria
        }else if(blOutros){
            ConnectOrg.RestAvaliacao(ConnectOrg.OAuth(GlobalConst.ConsKeyDrt,
                                                      GlobalConst.ConsSctDrt,
                                                      GlobalConst.UsuarioDrt,
                                                      GlobalConst.PasswordDrt,
                                                      GlobalConst.EndPointDrt), GlobalConst.EndPointDrt, GlobalConst.EndPoint_NewOutros);
        }
    }
    */
    public static void IntegrarDados(Boolean blApoio, Boolean blConselo, Boolean blConsela, Boolean blChefe, Boolean blOutros){
        if(blApoio){
            ConnectOrg.RestAvaliacao(ConnectOrg.OAuth(GlobalConst.ConsKeyDrt,
                                                      GlobalConst.ConsSctDrt,
                                                      GlobalConst.UsuarioDrt,
                                                      GlobalConst.PasswordDrt,
                                                      GlobalConst.EndPointDrt), GlobalConst.EndPointDrt, GlobalConst.EndPoint_NewApoio);
        }else if(blConselo){
            ConnectOrg.RestAvaliacao(ConnectOrg.OAuth(GlobalConst.ConsKeyDrt,
                                                      GlobalConst.ConsSctDrt,
                                                      GlobalConst.UsuarioDrt,
                                                      GlobalConst.PasswordDrt,
                                                      GlobalConst.EndPointDrt), GlobalConst.EndPointDrt, GlobalConst.EndPoint_NewConselo);
        }else if(blConsela){
            ConnectOrg.RestAvaliacao(ConnectOrg.OAuth(GlobalConst.ConsKeyDrt,
                                                      GlobalConst.ConsSctDrt,
                                                      GlobalConst.UsuarioDrt,
                                                      GlobalConst.PasswordDrt,
                                                      GlobalConst.EndPointDrt), GlobalConst.EndPointDrt, GlobalConst.EndPoint_NewConsela);
        //hefe nao entra na validacao, pois sera apenas avaliado pelo aplicativo de diretoria
        }else if(blOutros){
            ConnectOrg.RestAvaliacao(ConnectOrg.OAuth(GlobalConst.ConsKeyDrt,
                                                      GlobalConst.ConsSctDrt,
                                                      GlobalConst.UsuarioDrt,
                                                      GlobalConst.PasswordDrt,
                                                      GlobalConst.EndPointDrt), GlobalConst.EndPointDrt, GlobalConst.EndPoint_NewOutros);
        }
    }
}