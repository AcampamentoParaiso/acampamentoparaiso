global class ScheduleIntegrate implements Schedulable {
    global void execute(SchedulableContext SC) {
        Datetime lastHour = Datetime.now().addHours(-1).addMinutes(-15);
        List<Avaliacao__c> listAvalApoio = [SELECT RecordType.DeveloperName,
                                                   LastModifiedBy.Name
                                              FROM Avaliacao__c];
                                             //WHERE LastModifiedDate =: lastHour];
System.debug('Apoio.size: '+listAvalApoio.size());
        if(listAvalApoio.size()>0){
            Boolean blApoio = false;
            Boolean blConselo = false;
            Boolean blConsela = false;
            Boolean blChefe = false;
            Boolean blOutros = false;
            
            for(Avaliacao__c loopAval : listAvalApoio){
                if(loopAval.LastModifiedBy.Name.containsIgnoreCase(loopAval.RecordType.DeveloperName)){
                    if(loopAval.RecordType.DeveloperName ==  GlobalConst.Apoio){
                        blApoio = true;
                    }else if(loopAval.RecordType.DeveloperName ==  GlobalConst.Conselo){
                        blConselo = true;
                    }else if(loopAval.RecordType.DeveloperName ==  GlobalConst.Consela){
                        blConsela = true;
                    }else if(loopAval.RecordType.DeveloperName ==  GlobalConst.Chefe){
                        blChefe = true;
                    }else if(loopAval.RecordType.DeveloperName ==  GlobalConst.Outros){
                        blOutros = true;
                    }
                }
                AvaliacaoClass.IntegrarDados(blApoio, blConselo, blConsela, blChefe, blOutros);
            }
        }
    }
}