global class GlobalConst {
    
    //Variaveis Comum
    global static String Apoio = 'Apoio';
    global static String Chefe = 'Chefe';
    global static String Conselo = 'Conselheiro';
    global static String Consela = 'Conselheira';
    global static String Outros = 'Outros';
    global static String Ruim = 'Ruim';
    global static String Medio = 'Medio';
    global static String AvaliacaoC = 'Avaliacao__c';
    
    //Variaveis JSON
    global static String grantTypeJ = 'grant_type';
    global static String clientIdJ = 'client_id';
    global static String clientSecretJ = 'client_secret';
    global static String usernameJ = 'username';
    global static String passwordJ = 'password';
    global static String accTokenJ = 'access_token';
    global static String contTypeJ = 'Content-Type';
    global static String appJsonJ = 'application/json';
    global static String authoJ = 'Authorization';
    global static String oAuthJ = 'OAuth ';
    global static String AccessTokenJ = 'access_token';
    global static String RecordTypeJ = 'RecordType';
    global static String DeveloperNameJ = 'DeveloperName';
    global static String NameJ = 'Name';
    global static String NameCJ = 'Name__c';
    global static String FuncaoCJ = 'Funcao__c';
    global static String NovatoCJ = 'Novato__c';
    global static String EquipanteRJ = 'Equipante__r';
    global static String HorarioCJ = 'Horario__c';
    global static String SubmissaoChefiaCJ = 'Submissao_Chefia__c';
    global static String EnvolvimentoAcampantesCJ = 'Envolvimento_com_Acampantes__c';
    global static String BuscarCJ = 'Buscar__c';
    global static String BrincarCJ = 'Brincar__c';
    global static String BemDizerCJ = 'Bem_dizer__c';
    global static String ObservacaoCJ = 'Observacao__c';
    
    //Variaveis de EndPoint
    global static String EndPoint_NewApoio = '/services/apexrest/Avaliacao/NewApoio';
    global static String EndPoint_NewConselo = '/services/apexrest/Avaliacao/NewConselheiro';
    global static String EndPoint_NewConsela = '/services/apexrest/Avaliacao/NewConselheira';
    global static String EndPoint_NewOutros = '/services/apexrest/Avaliacao/NewOutros';
    global static String EndPoint_Auth = '/services/oauth2/token?';
    global static String EndPoint_Aval = '/services/apexrest/Avaliacao/';
    global static String EndPoint_Equi = '/services/apexrest/Equipante/';
    
    //Variaveis de HTTP
    global static String GET = 'GET';
    global static String POST = 'POST';
    global static String UTF8 = 'UTF-8';
    
    //Variaveis de HTTP Diretoria
    global static String ConsKeyDrt = '3MVG9szVa2RxsqBZzwVwqMA6gkztfEbN1Inmojs3o.WAbgudX80oJ4N22Ug.LMjTxs_iCQ5wyLEcOearh7JFl';
    global static String ConsSctDrt = '6640919023868019762';
    global static String UsuarioDrt = 'diretoria_paraiso@acampamento.com';
    global static String PasswordDrt = 'paradise2017';
    global static String EndPointDrt = 'https://na35.salesforce.com';
    
    //Variaveis de HTTP Chefe Apoio
    global static String ConsKeyApoio = '3MVG9szVa2RxsqBZFoXsMcazDr2MLU_D0g4qWELZpUHqz4pRFZmbcOY1_u5RyJSekok44eOrWTXjIhyKhYy7f';
    global static String ConsSctApoio = '2185981602076931142';
    global static String UsuarioApoio = 'paraiso@acampamento.com';
    global static String PasswordApoio = 'paradaise2017';
    global static String EndPointApoio = 'https://na35.salesforce.com';
    
    //Variaveis de HTTP Chefe Conselheiro
    global static String ConsKeyConselo = '3MVG9szVa2RxsqBZFoXsMcazDr2MLU_D0g4qWELZpUHqz4pRFZmbcOY1_u5RyJSekok44eOrWTXjIhyKhYy7f';
    global static String ConsSctConselo = '2185981602076931142';
    global static String UsuarioConselo = 'paraiso@acampamento.com';
    global static String PasswordConselo = 'paradaise2017';
    global static String EndPointConselo = 'https://na35.salesforce.com';
    
    //Variaveis de HTTP Chefe Conselheira
    global static String ConsKeyConsela = '3MVG9szVa2RxsqBZFoXsMcazDr2MLU_D0g4qWELZpUHqz4pRFZmbcOY1_u5RyJSekok44eOrWTXjIhyKhYy7f';
    global static String ConsSctConsela = '2185981602076931142';
    global static String UsuarioConsela = 'paraiso@acampamento.com';
    global static String PasswordConsela = 'paradaise2017';
    global static String EndPointConsela = 'https://na35.salesforce.com';
    
}